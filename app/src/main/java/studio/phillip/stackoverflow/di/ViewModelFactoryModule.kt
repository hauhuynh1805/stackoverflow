package studio.phillip.stackoverflow.di

import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import studio.phillip.stackoverflow.viewmodels.ViewModelProviderFactory

@Module
abstract class ViewModelFactoryModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelProviderFactory): ViewModelProvider.Factory
}