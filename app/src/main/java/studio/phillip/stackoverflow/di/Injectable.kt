package studio.phillip.stackoverflow.di

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable