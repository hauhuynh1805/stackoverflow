package studio.phillip.stackoverflow.di.auth

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import studio.phillip.stackoverflow.api.StackOverflowApiService
import studio.phillip.stackoverflow.persistence.UserDao
import studio.phillip.stackoverflow.repository.UserRepository

@Module
class AuthModule {

    @AuthScope
    @Provides
    fun provideFakeApiService(retrofitBuilder: Retrofit.Builder): StackOverflowApiService {
        return retrofitBuilder
            .build()
            .create(StackOverflowApiService::class.java)
    }

    @AuthScope
    @Provides
    fun provideAuthRepository(
        api: StackOverflowApiService,
        userDao: UserDao
    ): UserRepository {
        return UserRepository(
            api,
            userDao
        )
    }
}