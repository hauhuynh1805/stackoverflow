package studio.phillip.stackoverflow.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import studio.phillip.stackoverflow.di.auth.AuthFragmentBuildersModule
import studio.phillip.stackoverflow.di.auth.AuthModule
import studio.phillip.stackoverflow.di.auth.AuthScope
import studio.phillip.stackoverflow.di.auth.AuthViewModelModule
import studio.phillip.stackoverflow.ui.MainActivity

@Module
abstract class ActivityBuildersModule {

    @AuthScope
    @ContributesAndroidInjector(
        modules = [AuthModule::class, AuthFragmentBuildersModule::class, AuthViewModelModule::class]
    )
    abstract fun contributeAuthActivity(): MainActivity

}