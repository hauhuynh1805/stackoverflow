package studio.phillip.stackoverflow.di.auth

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import studio.phillip.stackoverflow.di.ViewModelKey
import studio.phillip.stackoverflow.ui.UserViewModel

@Module
abstract class AuthViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(UserViewModel::class)
    abstract fun bindAuthViewModel(authViewModel: UserViewModel): ViewModel

}