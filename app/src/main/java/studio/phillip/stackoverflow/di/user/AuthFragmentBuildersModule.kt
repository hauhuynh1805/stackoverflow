package studio.phillip.stackoverflow.di.auth

import dagger.Module
import dagger.android.ContributesAndroidInjector
import studio.phillip.stackoverflow.ui.DetailUserFragment
import studio.phillip.stackoverflow.ui.ListUsersFragment

@Module
abstract class AuthFragmentBuildersModule {

    @ContributesAndroidInjector()
    abstract fun contributeListUserFragment(): ListUsersFragment

    @ContributesAndroidInjector()
    abstract fun contributeDetailUserFragment(): DetailUserFragment
}